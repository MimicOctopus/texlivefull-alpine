FROM alpine:latest

RUN apk add --no-cache texlive-full python py-pip terminus-font && \
apk search -qe 'font-*' | xargs apk add --no-cache && \
pip --no-cache-dir install pygments

WORKDIR /build
